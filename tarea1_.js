// Pregunta: 1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: 

    // $ mongoimport -d students -c grades < grades.json

    /* Respuesta: 
        2023-10-05T12:00:39.874-0600    connected to: mongodb://localhost/
        2023-10-05T12:00:39.952-0600    800 document(s) imported successfully. 0 document(s) failed to import.
    */

// Pregunta: 2) ¿Cuántos registros arrojo el comando count?

    //$mongo

    // > show dbs
        /*Output: 
            admin         0.000GB
            config        0.000GB
            local         0.000GB
            my_data_base  0.000GB
            students      0.000GB <----- este el que usaremos para la practica
        */

    // > use students
        /*Output:
        switched to db students
        */

        db.grades.count()
        /*Output:
            800
        */

// Respuesta 2: El comando count arroja 800 registros

// Pregunta 3) Encuentra todas las calificaciones del estudiante con el id numero 4.

        db.grades.find({student_id:4})

    /*Output:
        { "_id" : ObjectId("50906d7fa3c412bb040eb587"), "student_id" : 4, "type" : "exam", "score" : 87.89071881934647 }
        { "_id" : ObjectId("50906d7fa3c412bb040eb588"), "student_id" : 4, "type" : "quiz", "score" : 27.29006335059361 }
        { "_id" : ObjectId("50906d7fa3c412bb040eb58a"), "student_id" : 4, "type" : "homework", "score" : 28.656451042441 }
        { "_id" : ObjectId("50906d7fa3c412bb040eb589"), "student_id" : 4, "type" : "homework", "score" : 5.244452510818443 }
    */

        db.grades.find({student_id:4}, {_id:0, score:1})

    /*
        { "score" : 87.89071881934647 }
        { "score" : 27.29006335059361 }
        { "score" : 28.656451042441 }
        { "score" : 5.244452510818443 }
    */

        db.grades.find({student_id:4}, {_id:0, type:1 ,score:1})

    /*
        { "type" : "exam", "score" : 87.89071881934647 }
        { "type" : "quiz", "score" : 27.29006335059361 }
        { "type" : "homework", "score" : 28.656451042441 }
        { "type" : "homework", "score" : 5.244452510818443 } 
    */

// Respuesta 5:
    // Exam: 87.89071881934647
    // Quiz: 27.29006335059361
    // Homework: 28.656451042441
    // Homework: 5.244452510818443

// Pregunta 4) ¿Cuántos registros hay de tipo exam?

        db.grades.find({type: "exam"}).count()

    // 200

// Respuesta 4: Existen 200 registros de tipo exam

// Pregunta 5) ¿Cuántos registros hay de tipo homework?

        db.grades.find({type: "homework"}).count()

    // 400

// Respuesta 5: Existen 400 registros de tipo homework

// Pregunta 6) ¿Cuántos registros hay de tipo quiz?

        db.grades.find({type: "quiz"}).count()

    // 200

// Resputa 6: Existen 200 registros de tipo quiz

// Pregunta 7) Elimina todas las calificaciones del estudiante con el id numero 3

    // Borrando todo el estudiante
            db.grades.deleteMany({student_id:3})

        /*Output:
            { "acknowledged" : true, "deletedCount" : 4 }
        */

            db.grades.find({student_id:3})

        // No muestra nada en terminal 
    
    // Borrando solo el el campo de calificaciones (para este borre la base de datos y la volvi a levantar por que en el anterior borre a todo el estudiando jeje)
            db.grades.updateMany({"student_id": 3}, {$unset:{"score": ""}})

        /** Output:
            { "acknowledged" : true, "matchedCount" : 4, "modifiedCount" : 4 }
        */

            db.grades.find({"student_id": 3})

        /**Output:
         *  { "_id" : ObjectId("50906d7fa3c412bb040eb583"), "student_id" : 3, "type" : "exam" }
            { "_id" : ObjectId("50906d7fa3c412bb040eb584"), "student_id" : 3, "type" : "quiz" }
            { "_id" : ObjectId("50906d7fa3c412bb040eb585"), "student_id" : 3, "type" : "homework" }
            { "_id" : ObjectId("50906d7fa3c412bb040eb586"), "student_id" : 3, "type" : "homework" }
         */

// Pregunta 8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?

        db.grades.find({ score : 75.29561445722392 }).count()

    // Output: 1

// Pregunta 9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

        db.grades.update({_id:ObjectId("50906d7fa3c412bb040eb591")},{$set:{"score":100}});

    /* Output:
        WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
    */    

// Pregunta 10) A qué estudiante pertenece esta calificación.

        db.grades.find({_id:ObjectId("50906d7fa3c412bb040eb591")})

    /* Output:
        { "_id" : ObjectId("50906d7fa3c412bb040eb591"), "student_id" : 6, "type" : "homework", "score" : 100 }
    */

        db.grades.find({"score":100})

    /* Output:
        { "_id" : ObjectId("50906d7fa3c412bb040eb591"), "student_id" : 6, "type" : "homework", "score" : 100 }
    */

// Respuesta 10: El estudiante al que le partene la calificacion es el numero 6