# Mongodb 101

Web Plataforms

## Unidad II

Programación Web del Lado del Servidor 

## Base de Datos

MongoDB shell version v6.1.1

## Autor

**Juan Antonio Díaz Fernández**

	Usuario: JUAN ANTONIO DIAZ FERNANDEZ

	Matricula: 348637

* Github de Juan: [Github](https://github.com/JuanDiazuwu)

* Gitlab de Juan: [Gitlab](https://gitlab.com/a348637)

## Profesor

Luis Antonio Ramírez Martínez

## Instrucciones

En esta tarea vamos a utilizar una colección de calificaciones de estudiantes, todas las respuestas deben de colocarse en un archivo llamado tarea_1.js y debe subirse a gitlab (siendo público el repositorio), utilizando comentarios, deben colocarse las preguntas, luego los comandos utilizados y de nuevo en comentarios las respuestas, al finalizar debe entregarse la liga de gitlab con la tarea.

1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json

2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count?

3) Encuentra todas las calificaciones del estudiante con el id numero 4.

4) ¿Cuántos registros hay de tipo exam?

5) ¿Cuántos registros hay de tipo homework?

6) ¿Cuántos registros hay de tipo quiz?

7) Elimina todas las calificaciones del estudiante con el id numero 3

8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?

9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

10) A qué estudiante pertenece esta calificación.

## Uso

1. Descargar la imagen de mongo:

```
$ docker pull mongo
```

2. Iniciar el contenedor:

```
$ docker create -d -p 27017:27017 --name <nombre_del_contenedor> mongo
```

3. Iniciar el contenedor:

```
$ docker start <nombre_del_contenedor>
```

4. Para hacer los ultimos comandos en uno solo:

```
$ docker run -d -p 27017:27017 --name monguito mongo
```

5. Agregar el archivo `grades.json` al contenedor:

```
$ docker cp ./grades.json <nombre_del_contenedor>:/
```

6. Entrar a la terminal del contenedor:

```
$ docker exec -it mongodb bash
```

7. Ya dentro del contenedor, ejecutar el siguiente comando:

```
# mongoimport -d students -c grades < grades.json
```

El comando `mongoimport` importa datos desde el archivo `grades.json` a la base de datos `students` y la colección `grades` en MongoDB, debería de mostrar algo como esto:

```
2023-10-05T12:00:39.874-0600    connected to: mongodb://localhost/
2023-10-05T12:00:39.952-0600    800 document(s) imported successfully. 0 document(s) failed to import.
```

8. Entrar al shell de mongo:

```
# mongosh
```

9. Seleccionar la base de datos `students`:

```
> use students
```

10. Para contar los documentos en la colección `grades` de la base de datos actual, en este caso `students`.

```
> db.grades.count()
```

Respuesta: 800

11. Encontar todas las calificaciones del estudiante con el id número 4:

```
> db.grades.find({student_id:4}, {_id:0, type:1 ,score:1})
```

12. Número de registros de tipo `exam`:

```
> db.grades.find({type: "exam"}).count()
```

Respuesta: 200

13. Número de registros de tipo `homework`:

```
> db.grades.find({type: "homework"}).count()
```

Respuesta: 400

14. Número de registros de tipo `quiz`:

```
> db.grades.find({type: "quiz"}).count()
```

Respuesta: 200

15. Eliminar todas las calificaciones del estudiante con el id número 3:

```
> db.grades.updateMany({"student_id": 3}, {$unset:{"score": ""}})
```

Para comprobar:

```
> db.grades.find({"student_id": 3})
```

16. Estudiantes que obtuvieron `75.29561445722392` en una tarea:

```
> db.grades.find({ score : 75.29561445722392 }).count()
```

Respuesta: 1

17. Actualizar las calificaciones del registro con el `UUID 50906d7fa3c412bb040eb591`` por 100

```
> db.grades.update({_id:ObjectId("50906d7fa3c412bb040eb591")},{$set:{"score":100}});
```

18. Pertenencia de la anterior calificación:

```
> db.grades.find({_id:ObjectId("50906d7fa3c412bb040eb591")})
```

El estudiante con el id número `6``.
Para comprobar:

```
> db.grades.find({"score":100})
```

## Script de Python

Hace toda la tarea con el siguiente comando:

```
$ python query.py
```