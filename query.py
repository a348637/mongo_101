import pymongo
from bson import ObjectId

client = pymongo.MongoClient('mongodb://localhost:27017/')

db = client['students']

collection = db['grades']

question_two = collection.count_documents({})
print(f'2.- {question_two}\n')

print('3.-')
question_three = collection.find({"student_id":4}, {"_id":0, "score":1})
for _ in question_three:
    print(_)
print()

question_four = collection.count_documents({"type":"exam"})
print(f'4.- {question_four}\n')

question_five = collection.count_documents({"type":"homework"})
print(f'5.- {question_five}\n')

question_six = collection.count_documents({"type":"quiz"})
print(f'6.- {question_six}\n')

question_seven = collection.update_many({"student_id": 3}, {"$unset":{"score": ""}})
print(f'7.- {question_seven}')
verification_seven = collection.find({"student_id":3})
for _ in verification_seven:
    print(_)
print()

question_eight = collection.count_documents({"score":75.29561445722392})
print(f'8.- {question_eight}\n')

question_nine = collection.update_one({"_id": ObjectId("50906d7fa3c412bb040eb591")},{"$set":{"score":100}})
verification_nine = collection.find({"_id":ObjectId("50906d7fa3c412bb040eb591")})
print(f'9.-')
for _ in verification_nine:
    print(_)
print()

question_ten = collection.find({"score":100})
print(f'10.-')
for _ in question_ten:
    print(_)